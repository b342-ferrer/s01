from django.contrib import admin

from .models import ToDoItem, ToDoEvent

admin.site.register(ToDoItem)
admin.site.register(ToDoEvent)