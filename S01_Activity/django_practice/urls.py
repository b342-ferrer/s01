#urls.py
# from django.urls import path
# from . import views


# # You can declare routes by using the path function and having 3 arguments within that function.
# # 1st argument - endpoint, 2nd argument - function to be triggered, 3rd argument - label for the route

# urlpatterns = [
# 	path("", views.index, name = "index")
# ]


from django.urls import path
from . import views


# You can declare routes by using the path function and having 3 arguments within that function.
# 1st argument - endpoint, 2nd argument - function to be triggered, 3rd argument - label for the route

urlpatterns = [
    path("", views.index, name = "index"),
    # /grocerylist/<groceryitem_id>
    path("<int:groceryitem_id>/", views.groceryitem, name='viewgroceryitem'),
    # /grocerylist/register
    path("register", views.register, name="register"),
    # /grocerylist/change_password
    path ('change_password', views.change_password, name = "change_password"),
    # /grocerylist/login
    path ('login', views.login_view, name = "login"),
    # /grocerylist/logout
    path ('logout', views.logout_view, name = "logout")
]